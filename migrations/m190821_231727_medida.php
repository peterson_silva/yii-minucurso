<?php

use yii\db\Migration;

/**
 * Class m190821_231727_medida
 */
class m190821_231727_medida extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('medida', [
            'id' => $this->primaryKey(),
            'nome' => $this->string()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('medida');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190821_231727_medida cannot be reverted.\n";

        return false;
    }
    */
}
