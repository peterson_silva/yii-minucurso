<?php

use yii\db\Migration;

/**
 * Class m190821_231738_ingrediente
 */
class m190821_231738_ingrediente extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ingrediente', [
            'id' => $this->primaryKey(),
            'nome' => $this->string()->notNull(),
            'medida_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey('ingrediente_medida_id_fk','ingrediente','medida_id','medida', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ingrediente');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190821_231738_ingrediente cannot be reverted.\n";

        return false;
    }
    */
}
