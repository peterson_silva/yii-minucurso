# Yii PHP Framework: aplicações rápidas e fáceis
## Instruções

1. Criem um repositorio em seu PC.
2. Faça o clone desse projeto em um repositório na sua maquina:
```
$ git clone https://gitlab.com/peterson_silva/yii-minucurso.git
$ cd yii-minicurso
``` 
4. Posteriormente crie uma branch:
```
$ git branch <seu_nome>
$ git checkout <seu_nome>
``` 
3. Crie um novo projeto Yii2 no repositório Git:
```
$ composer create-project --prefer-dist yiisoft/yii2-app-basic project
``` 
5. Dê push em sua branch:
```
$ git add .
$ git commit -m "Add application"
$ git push origin <seu_nome>
``` 

## Authors
@peterson_silva Péterson Silva: Developer PHP.

@thtmorais Matheus Morais: Developer PHP. 