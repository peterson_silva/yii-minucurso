<?php

use yii\db\Migration;

/**
 * Class m190821_231709_receita
 */
class m190821_231709_receita extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('receita', [
            'id' => $this->primaryKey(),
            'nome' => $this->string()->notNull(),
            'modo_preparo' => $this->text()->notNull(),
            'tempo' => $this->string(),
            'rendimento' => $this->string(),
            'observacoes' => $this->text(),
            'user_id' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('receita_user_id_fk', 'receita', 'user_id', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('receita');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190821_231709_receita cannot be reverted.\n";

        return false;
    }
    */
}
