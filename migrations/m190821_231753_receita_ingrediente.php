<?php

use yii\db\Migration;

/**
 * Class m190821_231753_receita_ingrediente
 */
class m190821_231753_receita_ingrediente extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('receita_ingrediente', [
            'id' => $this->primaryKey(),
            'receita_id' => $this->integer()->notNull(),
            'ingrediente_id' => $this->integer()->notNull(),
            'quantidade' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('receita_ingrediente_receita_id_fk','receita_ingrediente','receita_id','receita','id');
        $this->addForeignKey('ingrediente_ingrediente_receita_id_fk','receita_ingrediente','ingrediente_id','ingrediente','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('receita_ingrediente');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190821_231753_receita_ingrediente cannot be reverted.\n";

        return false;
    }
    */
}
